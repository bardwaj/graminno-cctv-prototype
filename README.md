# GramInno CCTV prototype

## Name
GramInno CCTV prototype

	Caveat: Exploring, probably (mis)using, gitlab,   
	to publicly document what is very likely to be   
	a largely brick-n-mortar project.    
    
## Description

Context: An aspiration of The GramInno project is to use technology with due  caution and due discretion to remain eco-sensitive and sustainable, while attempting to improve quality of life of residents, of a remote rural location in Maharashtra., India, where mobile network is unreliable at best and internet access is almost non-existent (till now, year 2023). More information about GramInno can be seen at :  https://graminno.org/about-us/
  
A common and serious problem that discourages present residents in this area from farming on their land is the unpredictable damage caused to crops by free roaming wildlife in the area. Well known animal visitors that cause crop damage are monkeys and wild-boar. Also leopards are known to visit at times seeking small domestic animals as food.
  
The GramInno CCTV prototype plans to gather verfiable data of such visits by wildlife on orchards and farms of the residents, duration of visits, time-of-day, etc, which information can then help plan eco-friendly and sustainable approaches to reduce such damage to crops and domestic animals, while also exploring automated responses to help shoo away such visitors in good time before much damage happens.  
  
## Badges
Not applicable at this time  
  
## Visuals
Not applicable at this time  

## Installation
1. Set up local-area-network (LAN) with DHCP based on a wifi router.  
2. Set up network-video-recorder (NVR) with 2-4 compatible wifi cameras.
3. Set up FOSS software laptop to work around unplanned issues with the NVR.
4. Set up public access the NVR imagery via mobile devices using wifi network.

## Usage
Once the system is operational, authorised local residents equipped with a   mobile (smart phone) device would be able access NVR imagery, without any specialist human intervention, while they are physically within the wifi router LAN area, and with NIL dependence on internet connectivity.

## Support  
Default support is from the author of this note at this time.  
All software deployed are open, free and public domain,   
and thus not tied down to any one indivdual.  
Software inherent to acquired appliances are as per their respective terms.   
We endeavour to adhere to open standards and open systems.
Ongoing support needs are expected to be minimal.   
The scope of this support aspect may be reviewed after 2-4 months of usage.
  
## Roadmap
This exercise will be a critical input into the design of the smart-street-lamp concept based on p2p data sharing network which is handled by another team at present within Accion, for which some developments and field trials were being done by Shahid Shaikh at Accion.
  
## Contributing
This project is currently open to person-hour contributions on-site initially to support erection and deployment, and later to support data review and analysis. Project prefers that all software and devices used conform to open standards, and requires all processes and results of the project are open, in public domain.

If you are unsure about how you can actually participate in this project, it can be useful to plan to spend a week on-site with prior coordination, to get first hand sense of the context and its challenges.

## Authors and acknowledgment  
This note is authored by V.K.Bharadwaj (consulting[at]bardwaj.in)
All activity within this project is coordinated and actioned by the author with oversight from Ashutosh Bijoor (ashutosh.bijoor[at]accionlabs.com). All appliances, devices, and similar resources for this project are provided by or through Accion. Once it is  commissioned, village residents will become active participants.

## License
This project is covered under the   
CC BY-SA 4.0 licence which can be seen here   
https://creativecommons.org/licenses/by-sa/4.0/  
  
## Project status
Activities 1, 2 and 3 cited under "Installation" above have been completed off-site. The materials have been moved on-site. Site preparation (mains cabling) and deployment on-site is yet to happen. This is status as on 31May2023.
  
  